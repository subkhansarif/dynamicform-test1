package id.subkhansarif.dynamicform.viewmodels

import id.subkhansarif.dynamicform.connection.models.FormOptions
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Created by Subkhan Sarif on 07/07/2018.
 */
data class FormViewModel(
        val key: String,
        val label: String,
        val typeField: String,
        val isRequired: Boolean = false,
        val minLength: Int?,
        val exactLength: Int?,
        val options: List<FormOptions>?
) {
    val viewType: ViewType by ViewTypeDelegate()
    val inputType: InputType by InputTypeDelegate()

    fun isValid(value: String, onError: (String) -> Unit): Boolean {
        if (isRequired && value.isEmpty()) {
            onError("$label cannot be empty")
            return false
        }
        minLength?.also {
            if (value.length < it) {
                onError("$label is too short.")
                return false
            }
        }
        exactLength?.also {
            if (value.length != it) {
                onError("$label needs to be $it long.")
                return false
            }
        }
        return true
    }
}

sealed class ViewType {
    class TypeEditText : ViewType()
    class TypeSpinner : ViewType()
    class TypeDatePicker : ViewType()
    class TypeButton: ViewType()
    class TypeElse : ViewType()
}

sealed class InputType {
    class TypeCommonText : InputType()
    class TypeEmailText : InputType()
    class TypePasswordText : InputType()
    class TypeNumberText : InputType()
    class TypeElse : InputType()
}

class ViewTypeDelegate : ReadOnlyProperty<FormViewModel, ViewType> {
    override fun getValue(thisRef: FormViewModel, property: KProperty<*>): ViewType {
        return when (thisRef.typeField) {
            "email", "password", "text" -> ViewType.TypeEditText()
            "date" -> ViewType.TypeDatePicker()
            "select" -> ViewType.TypeSpinner()
            "button" -> ViewType.TypeButton()
            else -> ViewType.TypeElse()
        }
    }
}

class InputTypeDelegate : ReadOnlyProperty<FormViewModel, InputType> {
    override fun getValue(thisRef: FormViewModel, property: KProperty<*>): InputType {
        return when (thisRef.typeField) {
            "email" -> InputType.TypeEmailText()
            "password" -> InputType.TypePasswordText()
            "text" -> when (thisRef.key) {
                "idCardNo" -> InputType.TypeNumberText()
                else -> InputType.TypeCommonText()
            }
            else -> InputType.TypeElse()
        }
    }
}
