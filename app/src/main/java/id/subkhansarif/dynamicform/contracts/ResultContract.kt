package id.subkhansarif.dynamicform.contracts

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
interface ResultContract {
    interface IResultView {
        fun onAttachView()
        fun onDetachView()
        fun beforeLoadResult()
        fun afterLoadResult()
        fun setTextView(text: String)
    }

    interface IResultPresenter {
        fun attachView(view: IResultView)
        fun detachView()
        fun loadResult()
    }
}