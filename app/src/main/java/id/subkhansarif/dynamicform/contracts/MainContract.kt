package id.subkhansarif.dynamicform.contracts

import id.subkhansarif.dynamicform.connection.models.FormResult
import id.subkhansarif.dynamicform.viewmodels.FormViewModel

/**
 * Created by Subkhan Sarif on 07/07/2018.
 */
interface MainContract {
    interface IMainView {
        fun onAttachView()
        fun onDetachView()
        fun beforefetchForm()
        fun afterFetchForm(success: Boolean = true, message: String? = null)
        fun addForm(formViewModels: List<FormViewModel>)
        fun beforeSubmit()
        fun afterSubmit()
    }

    interface IMainPresenter {
        fun attachView(view: IMainView)
        fun dettachView()
        fun fetchForm()
        fun submit(data: List<FormResult>)
    }
}