package id.subkhansarif.dynamicform.managers

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
class PrefManager {
    companion object {
        lateinit var PREFERENCE_NAME: String
        private var preference: SharedPreferences? = null

        fun init(context: Context) {
            PREFERENCE_NAME = context.applicationContext.packageName + ".PREFERENCE_MANAGER"
            preference = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        }
    }

    class StringManager {
        private val PREF_RESULT = "pref_result"

        fun saveResult(jsonString: String) {
            preference?.edit()?.apply {
                putString(PREF_RESULT, jsonString)
            }?.apply()
        }

        fun loadResult(): String? {
            return preference?.getString(PREF_RESULT, null)
        }
    }
}