package id.subkhansarif.dynamicform.utils

import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.app.AlertDialog
import java.util.*

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
fun Context.showDatePicker(myCalendar: Calendar, callback: (calendar: Calendar) -> Unit) {
    DatePickerDialog(this, { datePicker, year, month, day ->
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, month)
        myCalendar.set(Calendar.DAY_OF_MONTH, day)
        callback(myCalendar)
    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
}

fun Context.showDialog(message: String) {
    val builder = AlertDialog.Builder(this)
    builder.setMessage(message)
    builder.setPositiveButton("OK") { dialogInterface, i ->
    }
    builder.show()
}