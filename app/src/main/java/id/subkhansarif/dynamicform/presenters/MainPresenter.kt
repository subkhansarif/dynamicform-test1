package id.subkhansarif.dynamicform.presenters

import android.util.Log
import com.google.gson.Gson
import id.subkhansarif.dynamicform.connection.Connection
import id.subkhansarif.dynamicform.connection.Server
import id.subkhansarif.dynamicform.connection.models.FormResponse
import id.subkhansarif.dynamicform.connection.models.FormResult
import id.subkhansarif.dynamicform.contracts.MainContract
import id.subkhansarif.dynamicform.managers.PrefManager
import id.subkhansarif.dynamicform.viewmodels.FormViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Subkhan Sarif on 07/07/2018.
 */
class MainPresenter : MainContract.IMainPresenter {
    private val server: Server by lazy { Connection().createServer() }
    private var view: MainContract.IMainView? = null

    override fun attachView(view: MainContract.IMainView) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }

    override fun fetchForm() {
        view?.beforefetchForm()
        server.getForm().enqueue(object : Callback<FormResponse> {
            override fun onFailure(call: Call<FormResponse>?, t: Throwable?) {
                view?.afterFetchForm(false, t?.message)
            }

            override fun onResponse(call: Call<FormResponse>?, response: Response<FormResponse>?) {
                Log.i("FetchForm", "Response: ${response?.body()}")
                if (response != null) {
                    val body = response.body()
                    if (body != null) {
                        handleResponse(body)
                        view?.afterFetchForm()
                    } else {
                        val errorBody = response.errorBody()
                        if (errorBody != null) {
                            view?.afterFetchForm(false, errorBody.string())
                        } else {
                            view?.afterFetchForm(false, "Something went wrong. Please try again later.")
                        }
                    }
                } else {
                    view?.afterFetchForm(false, "Something went wrong. Please try again later.")
                }
            }
        })
    }

    private fun handleResponse(formResponse: FormResponse) {
        formResponse.questions.map {
            FormViewModel(
                    it.key,
                    it.label,
                    it.typeField,
                    it.validation?.required ?: false,
                    it.validation?.minLength,
                    it.validation?.exactLength,
                    it.options
            )
        }.also {

            // add one viewmodel at the end of the list as submit button
            val buttonedList: MutableList<FormViewModel> = it.toMutableList()
            val buttonView = FormViewModel(
                    "submit",
                    "Submit",
                    "button",
                    false,
                    null,
                    null,
                    null
            )
            buttonedList.add(buttonedList.size, buttonView)

            // add form to view
            view?.addForm(buttonedList)
        }
    }

    override fun submit(data: List<FormResult>) {
        view?.beforeSubmit()
        val jsonString = Gson().toJson(data)
        PrefManager.StringManager().saveResult(jsonString)
        view?.afterSubmit()
    }
}