package id.subkhansarif.dynamicform.presenters

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.subkhansarif.dynamicform.connection.models.FormResult
import id.subkhansarif.dynamicform.contracts.ResultContract
import id.subkhansarif.dynamicform.managers.PrefManager

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
class ResultPresenter : ResultContract.IResultPresenter {

    var view: ResultContract.IResultView? = null

    override fun attachView(view: ResultContract.IResultView) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }

    override fun loadResult() {
        view?.beforeLoadResult()
        val result: List<FormResult> = Gson().fromJson(PrefManager.StringManager().loadResult(), object : TypeToken<List<FormResult>>() {}.type)
        var stringResult = ""
        result.forEach {
            stringResult += "${it.key}\t:\t${it.value}\n"
        }
        view?.setTextView(stringResult)
        view?.afterLoadResult()
    }
}