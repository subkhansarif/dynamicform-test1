package id.subkhansarif.dynamicform.views

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.text.InputFilter
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import id.subkhansarif.dynamicform.R
import id.subkhansarif.dynamicform.connection.models.FormOptions
import id.subkhansarif.dynamicform.utils.showDatePicker
import id.subkhansarif.dynamicform.viewmodels.FormViewModel
import id.subkhansarif.dynamicform.viewmodels.InputType
import id.subkhansarif.dynamicform.viewmodels.ViewType
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Subkhan Sarif on 07/07/2018.
 */
class FormView(val ctx: Context, val viewModel: FormViewModel, val onSubmit: (() -> Unit)?) : LinearLayout(ctx) {

    val viewLabel: TextView
    val viewInputLayoutContainer: TextInputLayout
    val viewEditText: TextInputEditText
    val viewSpinner: Spinner
    val viewSelectDate: TextView
    val viewSubmit: Button

    init {
        // set layout orientation and gravity
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.CENTER_VERTICAL

        // inflate layout
        val inflater = LayoutInflater.from(ctx)
        inflater.inflate(R.layout.layout_form, this, true)

        // initialize view
        viewLabel = getChildAt(0) as TextView
        viewInputLayoutContainer = getChildAt(1) as TextInputLayout
        viewEditText = viewInputLayoutContainer.findViewById(R.id.edit_text)
        viewSpinner = getChildAt(2) as Spinner
        viewSelectDate = getChildAt(3) as TextView
        viewSubmit = getChildAt(4) as Button

        viewLabel.text = viewModel.label

        // handle view
        when (viewModel.viewType) {
            is ViewType.TypeEditText -> {
                handleEditText()
            }
            is ViewType.TypeSpinner -> {
                handleSpinner()
            }
            is ViewType.TypeDatePicker -> {
                handleDatePicker()
            }
            is ViewType.TypeButton -> {
                handleButton()
            }
        }
    }

    private fun handleEditText() {
        viewLabel.visibility = View.GONE
        viewInputLayoutContainer.visibility = VISIBLE
        viewSpinner.visibility = GONE
        viewSelectDate.visibility = GONE
        viewSubmit.visibility = GONE

        // add tag
        viewEditText.tag = viewModel.key

        viewInputLayoutContainer.hint = viewModel.label

        // set input type
        when (viewModel.inputType) {
            is InputType.TypeNumberText -> viewEditText.inputType = android.text.InputType.TYPE_CLASS_NUMBER
            is InputType.TypePasswordText -> {
                viewEditText.inputType = android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
                viewEditText.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            is InputType.TypeEmailText -> viewEditText.inputType = android.text.InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        }

        // set input text length
        viewModel.exactLength?.let {
            val filterArray = arrayOfNulls<InputFilter>(1)
            filterArray[0] = InputFilter.LengthFilter(it)
            viewEditText.filters = filterArray
        }
    }

    private fun handleSpinner() {
        viewLabel.visibility = View.VISIBLE
        viewInputLayoutContainer.visibility = GONE
        viewSpinner.visibility = VISIBLE
        viewSelectDate.visibility = GONE
        viewSubmit.visibility = GONE

        // add tag
        viewSpinner.tag = viewModel.key

        // set adapter
        viewModel.options?.let {
            viewSpinner.adapter = MySpinnerAdapter(ctx, it)
        }
    }

    private fun handleDatePicker() {
        viewLabel.visibility = View.VISIBLE
        viewInputLayoutContainer.visibility = GONE
        viewSpinner.visibility = GONE
        viewSelectDate.visibility = VISIBLE
        viewSubmit.visibility = GONE

        // add tag
        viewSelectDate.tag = viewModel.key

        viewSelectDate.setOnClickListener {
            // show date picker
            val calendar = Calendar.getInstance()
            ctx.showDatePicker(calendar) {
                val dateString = SimpleDateFormat.getDateInstance().format(it.time)
                viewSelectDate.text = dateString
            }
        }
    }

    private fun handleButton() {
        viewLabel.visibility = GONE
        viewInputLayoutContainer.visibility = GONE
        viewSpinner.visibility = GONE
        viewSelectDate.visibility = GONE
        viewSubmit.visibility = VISIBLE

        viewSubmit.text = viewModel.label
        viewSubmit.setOnClickListener { onSubmit?.invoke() }

    }
}

class MySpinnerAdapter(val context: Context, initData: List<FormOptions>) : BaseAdapter() {

    var data: MutableList<FormOptions> = initData.toMutableList()

    fun updateData(newData: List<FormOptions>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    override fun getView(position: Int, view: View?, container: ViewGroup?): View {
        val v = view
                ?: LayoutInflater.from(context).inflate(R.layout.layout_simple_spinner, container, false)
        handleView(position, v)
        return v
    }

    override fun getItem(position: Int): FormOptions {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }

    private fun handleView(position: Int, view: View) {
        view.findViewById<TextView>(R.id.txt_label).text = data[position].label
    }
}