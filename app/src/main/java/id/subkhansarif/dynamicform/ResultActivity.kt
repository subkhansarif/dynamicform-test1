package id.subkhansarif.dynamicform

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import id.subkhansarif.dynamicform.contracts.ResultContract
import id.subkhansarif.dynamicform.presenters.ResultPresenter
import kotlinx.android.synthetic.main.activity_result.*

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
class ResultActivity : AppCompatActivity(), ResultContract.IResultView {

    val presenter = ResultPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        onAttachView()

        // load result
        presenter.loadResult()
    }

    override fun onAttachView() {
        presenter.attachView(this)
    }

    override fun onDetachView() {
        presenter.detachView()
    }

    override fun beforeLoadResult() {
        progress_bar.visibility = VISIBLE
    }

    override fun afterLoadResult() {
        progress_bar.visibility = GONE
    }

    override fun setTextView(text: String) {
        txt_result.text = text
    }
}