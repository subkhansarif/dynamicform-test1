package id.subkhansarif.dynamicform

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Spinner
import android.widget.TextView
import id.subkhansarif.dynamicform.connection.models.FormOptions
import id.subkhansarif.dynamicform.connection.models.FormResult
import id.subkhansarif.dynamicform.contracts.MainContract
import id.subkhansarif.dynamicform.presenters.MainPresenter
import id.subkhansarif.dynamicform.utils.showDialog
import id.subkhansarif.dynamicform.viewmodels.FormViewModel
import id.subkhansarif.dynamicform.viewmodels.ViewType
import id.subkhansarif.dynamicform.views.FormView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.IMainView {

    val presenter = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // attach the view onto presenter
        onAttachView()

        // fetch question form
        presenter.fetchForm()
    }

    override fun onAttachView() {
        presenter.attachView(this)
    }

    override fun onDetachView() {
        presenter.dettachView()
    }

    override fun beforefetchForm() {
        // show Progress bar
        progress_bar.visibility = VISIBLE
        main_container.visibility = GONE
    }

    override fun afterFetchForm(success: Boolean, message: String?) {
        // hide progress bar
        progress_bar.visibility = GONE
        main_container.visibility = VISIBLE
        // show error message if not success
        if (!success) {
            message?.also { showDialog(it) }
        }
    }

    override fun addForm(formViewModels: List<FormViewModel>) {
        val onSubmit = object : () -> Unit {
            override fun invoke() {
                gatherResult(formViewModels)
            }
        }

        val formViews = formViewModels.map {
            FormView(this, it, onSubmit)
        }

        formViews.forEach {
            main_container.addView(it)
        }
    }

    private fun gatherResult(viewModels: List<FormViewModel>) {
        var isDone = true
        val resuls: MutableList<FormResult> = ArrayList()
        for (i in 0 until viewModels.size) {
            val it = viewModels[i]
            val formResult = FormResult(it.key, "")
            when (it.viewType) {
                is ViewType.TypeEditText -> {
                    formResult.value = main_container.findViewWithTag<TextInputEditText>(it.key).text.toString()
                }
                is ViewType.TypeSpinner -> {
                    formResult.value = (main_container.findViewWithTag<Spinner>(it.key).selectedItem as FormOptions).value
                }
                is ViewType.TypeDatePicker -> {
                    formResult.value = main_container.findViewWithTag<TextView>(it.key).text.toString()
                }
            }

            if (it.viewType !is ViewType.TypeButton) {
                // check if value is valid
                val isValid = it.isValid(formResult.value) {
                    showDialog(it)
                }
                // add to results list
                if (isValid) {
                    resuls.add(formResult)
                } else {
                    isDone = false
                    break
                }
            }
        }
        Log.d("GatherResult", "results: $resuls")
        if (isDone) presenter.submit(resuls)
    }

    override fun beforeSubmit() {
        progress_bar.visibility = VISIBLE
        main_container.visibility = GONE
    }

    override fun afterSubmit() {
        progress_bar.visibility = GONE
        main_container.visibility = VISIBLE
        startActivity(Intent(this, ResultActivity::class.java))
    }
}
