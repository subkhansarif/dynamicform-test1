package id.subkhansarif.dynamicform.connection.models

/**
 * Created by Subkhan Sarif on 07/07/2018.
 */
data class FormResponse(val version: String, val questions: List<FormQuestion>)

data class FormQuestion(val key: String, val label: String, val typeField: String, val validation: FormValidation?, val options: List<FormOptions>?)

data class FormValidation(val required: Boolean, val minLength: Int?, val exactLength: Int?)

data class FormOptions(val label: String, val value: String)