package id.subkhansarif.dynamicform.connection

import id.subkhansarif.dynamicform.connection.models.FormResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

/**
 * Created by Subkhan Sarif on 07/07/2018.
 */

interface Server {
    companion object {
        const val GET_FORM = "/amarthaid/test-mobile/master/Problem-1/question.json"
    }

    @GET(GET_FORM)
    fun getForm(): Call<FormResponse>
}

class Connection {
    private val BASE_URL = "https://raw.githubusercontent.com/"
    fun createServer(): Server {
        val client = OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        return retrofit.create(Server::class.java)
    }
}