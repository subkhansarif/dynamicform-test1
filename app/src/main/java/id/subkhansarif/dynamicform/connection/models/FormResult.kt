package id.subkhansarif.dynamicform.connection.models

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
data class FormResult(val key: String, var value: String)