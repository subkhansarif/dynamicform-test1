package id.subkhansarif.dynamicform

import android.app.Application
import id.subkhansarif.dynamicform.managers.PrefManager

/**
 * Created by Subkhan Sarif on 08/07/2018.
 */
class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        PrefManager.init(this)
    }
}